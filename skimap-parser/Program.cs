﻿using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace skimap_parser
{
    class Program
    {
        static MongoCollection<BsonDocument> _collection;

        static void Main(string[] args)
        {
            Ini();

            XmlTextReader reader = new XmlTextReader("../../skimap.xml");

            while (reader.Read())
            {
                // Do some work here on the data.
                if (reader.Name == "skiArea" && reader.NodeType == XmlNodeType.Element)
                {
                    var id = reader.GetAttribute("id");

                    var resp = Query(id);

                    var doc = ParseResponse(resp);

                    Write(doc);
                }
            }

            Console.ReadLine();            
        }

        static string Query(string id)
        {
            WebRequest request = WebRequest.Create("https://skimap.org/SkiAreas/view/" + id + ".json");

            var response = request.GetResponse();

            // Get the stream containing content returned by the server.
            Stream dataStream = response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);
            // Read the content.
            string responseFromServer = reader.ReadToEnd();
            // Display the content.
            Console.WriteLine(responseFromServer);
            // Clean up the streams and the response.
            reader.Close();
            response.Close();

            return responseFromServer;
        }

        static BsonDocument ParseResponse(string json)
        {
            //return JsonConvert.DeserializeObject<dynamic>(Response);
            return MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(json); 
        }

        static void Ini()
        {
            var connectionString = "mongodb://localhost";
            var client = new MongoClient(connectionString);
            var server = client.GetServer();
            var database = server.GetDatabase("ride-better-dev");

            _collection = database.GetCollection<BsonDocument>("skimap");
        
        }

        static void Write(BsonDocument Obj)
        {
            _collection.Insert(Obj);
        }

    }
}
