﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace google_geo_scrapper
{
    class Program
    {
        static string outFileName = "../../out/ride-forecast-geo-result.txt";
        static string inFileName = "../../in/ride-forecast-result.txt";
        static string outNotFound = "../../out/ride-forecast-geo-404-result.txt";

        static void Main(string[] args)
        {
            int counter = -1;
            string line;

            /*
            System.IO.StreamReader file = new System.IO.StreamReader(inFileName);
            while ((line = file.ReadLine()) != null)
            {
                counter++;

                //if (counter <= 6194) continue;

                if (string.IsNullOrEmpty(line)) continue;

                var spts = line.Split(new string[] { "|" }, StringSplitOptions.None);
                
                var s = string.Join(", ", spts[2], spts[3], spts[4]);

                var responseFromServer = Query(s);

                var obj = ParseResponse(responseFromServer);

                Write(counter, obj);

                System.Threading.Thread.Sleep(500);
            }
             */
            System.IO.StreamReader file = new System.IO.StreamReader(outFileName);
            while ((line = file.ReadLine()) != null)
            {
                var spts = line.Split(new string[] { "|" }, StringSplitOptions.None);
                if (spts.Length == 1)
                {
                    System.IO.File.AppendAllLines(outNotFound, spts);
                }
            }

            Console.ReadKey();
        }

        static string Query(string Addr)
        {
            WebRequest request = WebRequest.Create("https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyCX5wneuz2g5BBbisizU_dSG4fnKnSXeeg&address=" + Addr);

            var response = request.GetResponse();

            // Get the stream containing content returned by the server.
            Stream dataStream = response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);
            // Read the content.
            string responseFromServer = reader.ReadToEnd();
            // Display the content.
            Console.WriteLine(responseFromServer);
            // Clean up the streams and the response.
            reader.Close();
            response.Close();

            return responseFromServer;
        }

        static dynamic ParseResponse(string Response)
        {
            var resp = JsonConvert.DeserializeObject<dynamic>(Response);

            if (resp.status == "OK")
            {
                var res = resp.results[0];


                //var localityAddr = (dynamic)((JArray)res.address_components).FirstOrDefault(p => p.Value<JArray>("types").Any(s => s.Value<string>() == "locality"));
                var countryAddr = (dynamic)((JArray)res.address_components).FirstOrDefault(p => p.Value<JArray>("types").Any(s => s.Value<string>() == "country"));
                var regionAddr = (dynamic)((JArray)res.address_components).FirstOrDefault(p => p.Value<JArray>("types").Any(s => s.Value<string>() == "administrative_area_level_1"));
                var cityAddr = (dynamic)((JArray)res.address_components).FirstOrDefault(p => p.Value<JArray>("types").Any(s => s.Value<string>() == "locality"));
                //var cityAddr = (dynamic)((JArray)res.address_components).FirstOrDefault(p => p.Value<JArray>("types").Any(s => s.Value<string>() == "administrative_area_level_2"));

                return new
                {
                    name = res.formatted_address,
                    lat = res.geometry.location.lat,
                    lng = res.geometry.location.lng,
                    //locality = localityAddr != null && localityAddr.long_name != null ? new { code = localityAddr.short_name, name = localityAddr.long_name } : null,
                    country = countryAddr != null && countryAddr.long_name != null ? new { code = countryAddr.short_name, name = countryAddr.long_name } : null,
                    region = regionAddr != null && regionAddr.long_name != null ? new { code = regionAddr.short_name, name = regionAddr.long_name } : null,
                    city = cityAddr != null && cityAddr.long_name != null ? new { code = cityAddr.short_name, name = cityAddr.long_name } : null
                };
            }
            else
            {
                if (resp.status == "ZERO_RESULTS" || resp.status == "REQUEST_DENIED")
                {
                    return null;
                }
                else
                {
                    throw new Exception(resp.status);
                }
            }
        }

        static void Write(int idx, dynamic obj)
        {
            var str = idx.ToString();

            if (obj != null)
            {

                //var loc_code = obj.locality == null ? "" : obj.locality.code;
                //var loc_name = obj.locality == null ? "" : obj.locality.name;
                var country_code = obj.country == null ? "" : obj.country.code;
                var country_name = obj.country == null ? "" : obj.country.name;
                var region_code = obj.region == null ? "" : obj.region.code;
                var region_name = obj.region == null ? "" : obj.region.name;
                var city_code = obj.city == null ? "" : obj.city.code;
                var city_name = obj.city == null ? "" : obj.city.name;

                str = str + "|" +
                    obj.name + "|" +
                    obj.lat + "|" +
                    obj.lng + "|" +
                    country_code + "|" +
                    country_name + "|" +
                    region_code + "|" +
                    region_name + "|" +
                    city_code + "|" +
                    city_name + "|";
                /*
                +           
                loc_code + "|" +
                loc_name + "|";
                 */
            }

            System.IO.File.AppendAllLines(outFileName, new string[] { str });
        }
    }
}
