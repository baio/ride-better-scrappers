﻿using Abot.Core;
using Abot.Crawler;
using Abot.Poco;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ride_better_scrapers_utils;

namespace surf_forecast_spots_scraper
{
    class Program
    {
        static int cnt;

        static void Main(string[] args)
        {
            StartWrite();

            Craw();

            StopWrite();

            Console.ReadKey();
        }

        public static void Craw()
        {

            log4net.Config.XmlConfigurator.Configure();

            IWebCrawler crawler;

            crawler = GetManuallyConfiguredWebCrawler();

            crawler.PageCrawlCompletedAsync += crawler_ProcessPageCrawlCompleted;

            //Uri uriToCrawl = new Uri("http://www.snow-forecast.com/resorts/Zavjalikha");//("http://www.snow-forecast.com/countries");
            Uri uriToCrawl = new Uri("http://www.snow-forecast.com/countries");
            crawler.Crawl(uriToCrawl);

            //stream.Close();

        }


        private static IWebCrawler GetManuallyConfiguredWebCrawler()
        {
            //Create a config object manually

            var linkParser = new HapHyperLinkParser();

            CrawlConfiguration crawlConfig = new CrawlConfiguration();
            crawlConfig.MaxPagesToCrawl = 900000;

            return new PoliteWebCrawler(crawlConfig, null, null, null, null, new MyLinkParser(), null, null, null);
        }


        private class MyLinkParser : HapHyperLinkParser
        {

            public override System.Collections.Generic.IEnumerable<Uri> GetLinks(CrawledPage crawledPage)
            {
                if (crawledPage.Uri.AbsoluteUri == "http://www.snow-forecast.com/countries")
                {
                    return crawledPage.CsQueryDocument["table.mapctrytab tr td a"]
                        .Select(p => new Uri(crawledPage.Uri.Scheme + "://" + crawledPage.Uri.Host + p.GetAttribute("href"))).ToArray();
                }
                else if (Regex.IsMatch(crawledPage.Uri.AbsoluteUri, @"http://www.snow-forecast.com/countries/.*/resorts"))
                {                    
                    return crawledPage.CsQueryDocument["table.digest-table tr td.name a"]
                        .Select(p => new Uri(crawledPage.Uri.Scheme + "://" + crawledPage.Uri.Host + p.GetAttribute("href"))).ToArray();
                    
                }
                else
                {
                    return new Uri[0];
                }
            }
        }


        static void crawler_ProcessPageCrawlCompleted(object sender, PageCrawlCompletedArgs e)
        {
            if (Regex.IsMatch(e.CrawledPage.Uri.AbsoluteUri, "http://www.snow-forecast.com/resorts/.*"))
            {

                var loc = e.CrawledPage.CsQueryDocument["p.breadcrumbs"].Text();
                var nearest = e.CrawledPage.CsQueryDocument["table[width=\"95%\"] table:eq(1) td"].First().Text();

                var locs = loc.SplitEx(new[] { "\n" }).Select(p => Regex.Replace(p, @"\)|\(|\d|>", "", RegexOptions.IgnoreCase).Trim()).ToArray();
                var ns = nearest.SplitEx(new[] { "\n" }).Select(p => Regex.Replace(p, @",", "", RegexOptions.IgnoreCase).Trim()).ToArray();

                var code = e.CrawledPage.Uri.Segments[2];
                var countryAndRegion = locs[1];
                var spot = locs[2];
                var cr = countryAndRegion.SplitEx(new[] { "-" });
                var country = cr[0].Trim();
                var region = "";
                if (cr.Length > 1)
                    region = cr[1].Trim();

                var city = ns[0];
                var cityCountry = ns[1];
                var line = string.Join("|",
                    code, country, region, spot, cityCountry, city
                );

                Write(line);
            }
        }

        static string FileName = "../../out/ride-forecast-result.txt";
        static TextWriter writer;

        static void StartWrite()
        {
            if (File.Exists(FileName))
            {
                File.Delete(FileName);
            }

            writer = File.CreateText(FileName);
        }

        static void StopWrite()
        {
            writer.Close();
        }

        static void Write(string Line)
        {
            System.Console.WriteLine(Line);
            TextWriter.Synchronized(writer).WriteLine((cnt++) + "|" + Line);
        }

    }
}
