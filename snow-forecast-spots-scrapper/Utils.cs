﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ride_better_scrapers_utils
{
    public static class StringExtensions
    {
        private static TimeZone[] _tzs;

        public static string Clean(this string Str, string RmString = null)
        {
            return Clean(Str, RmString == null ? new string[0] : new string[] { RmString });
        }

        public static string Clean(this string Str, string[] RmStrings)
        {
            foreach (var str in RmStrings)
                Str = Str.Replace(str, "");

            Str = Str.Replace("\r", " ").Replace("\n", " ");

            Str = Regex.Replace(Str, @"\s{2,}", " ");

            return Str.Trim(' ', ',');
        }

        public static string[] SplitEx(this string Str, string[] Separs = null)
        {
            if (Separs == null)
                Separs = new string[] { ",", "–" };
            return Str.Split(Separs, StringSplitOptions.RemoveEmptyEntries).Select(p => p.Trim()).ToArray();
        }


        public static int? ParseInt(this string Str)
        {
            int i;

            if (int.TryParse(Str, out i))
                return i;
            else
                return null;
        }

        public static float? ParseFloat(this string Str)
        {
            float i;

            if (float.TryParse(Str, out i))
                return i;
            else
                return null;
        }

        public static string ParseStr(this string Str, params string[] AsNull)
        {
            return (AsNull.Contains(Str) ? null : Str).Clean();
        }

        /*
        private static void IniTZ()
        {
            if (_tzs == null)
            {
                var r = JsonConvert.DeserializeObject<JArray>(Resource1.TZ);

                _tzs = r.Select(p =>
                {
                    var arr = p.Values<string>().ToArray();

                    return new TimeZone { Abbr = arr[0], Code = arr[2], Name = arr[1], ShortCode = arr[2].Replace("UTC", "") };
                }).ToArray();
            }
        }

        /// <summary>
        /// Get time zone from abbreviation, such as CEST
        /// </summary>
        public static TimeZone ParseTimeZoneFromAbbr(this string Abbr)
        {
            IniTZ();

            return _tzs.FirstOrDefault(p => p.Abbr == Abbr);
        }

        public static TimeZone ParseTimeZoneFromName(this string Name)
        {
            IniTZ();

            var cleanName = Name.Clean("Standard");
            var na1 = cleanName + " (North America)";
            var na2 = Name + " (North America)";

            return _tzs.FirstOrDefault(p => p.Name == cleanName || p.Name == Name || p.Name == na1 || p.Name == na2);
        }
         */

        static string Trim(string Str)
        {
            return Regex.Replace(Str, @"^(l'|le|de|a|g\.)\s+", "", RegexOptions.IgnoreCase).Trim(' ', '(', ')');
        }

        public static string GetCode(this string Str, int Idx = 0, string Alter = null)
        {
            string res = "";

            if (!string.IsNullOrEmpty(Str) && Str.Length > 3)
            {
                var spts = Str.Split(new[] { " ", "-" }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(p => Trim(p)).Where(p => !string.IsNullOrEmpty(p)).ToArray();

                //res = string.Join("", spts);

                if (spts.Length >= 2)
                    res = string.Join("", spts.Take(spts.Length - 1).Select(p => Regex.IsMatch(p, @"^\d+$") ? p : p.First().ToString()));

                res = (res + string.Join("", spts[spts.Length - 1].Take((res.Length > 0 ? 1 : 2) + Idx).ToArray())).ToUpper();

            }
            else
            {
                res = string.IsNullOrEmpty(Str) ? "$NE" : Str.ToUpper();
            }

            if (!string.IsNullOrEmpty(Alter) && Encoding.UTF8.GetByteCount(res) != res.Length)
            {
                res = Alter.GetCode(Idx);
            }

            return res.Normalize(NormalizationForm.FormC);
        }

    }

    public class TimeZone
    {
        public string Abbr { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

        public string ShortCode { get; set; }
    }
}
